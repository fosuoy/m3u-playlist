## EXTM3U Generator
Generates #EXTM3U file out of valid video streams found from existing #EXTM3U files

##Guide:

git clone this_repo

cd this_repo

install dependencies:
`#> pip3 install -r requirements.txt`

Then run
`./m3u-output.py`
Use the outputed:
`m3u-output.m3u`
file
